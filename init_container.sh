#!/bin/bash
##
# Copy default config files to mounted volumes

# First work around systemd not giving us the environment
for e in $(tr "\000" "\n" < /proc/1/environ); do
    eval "export $e"
done

env

# Always export the docs dir to the volume mounted from the host.
cp -pRuv /usr/share/doc/zoneminder-common.default/* /usr/share/doc/zoneminder-common/

# Copy other config files if they are missing or an update is forced.
for CUR_DIR in $(tr ',' '\n' <<< "${VOLUMES}") ; do \
    if [ -f ${CUR_DIR}/.forceinit ] || [ ! "$(ls -A ${CUR_DIR}/)" ]; then
        if [ -d /usr/share/doc/zoneminder-common.default/config${CUR_DIR} ]; then
            cp -pRv /usr/share/doc/zoneminder-common.default/config${CUR_DIR}/* ${CUR_DIR}/
        fi
    fi
done

sed -i -e "s/^ZM_DB_NAME=.*/ZM_DB_NAME=${MARIADB_DB}/" -e "s/^ZM_DB_USER=.*/ZM_DB_USER=${MARIADB_USER}/" -e "s/^ZM_DB_PASS=.*/ZM_DB_PASS=${MARIADB_PWD}/" /etc/zm/zm.conf

USER_TABLE_EXISTS=$(mysql -u"${MARIADB_USER}" -p"${MARIADB_PWD}" -D"${MARIADB_DB}" -e "show tables like 'Users';")
[ -z "${USER_TABLE_EXISTS}" ] && mysql -v -u${MARIADB_USER} -p${MARIADB_PWD} -D${MARIADB_DB} < /usr/share/zoneminder/db/zm_create.sql

touch /etc/init_done

