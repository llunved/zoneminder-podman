#!/bin/bash

REGISTRY=${REGISTRY:-"edmobuttes.int.bos.llnvd.net"}
HWARCH=${HWARCH:-`uname -m`}
#VOL_PREFIX="/var/lib/pod-zm"
HTTPD_IMG="${REGISTRY}/`uname -m`/httpd:latest"
ZM_IMG="${REGISTRY}/`uname -m`/zoneminder:latest"
MARIADB_IMG="${REGISTRY}/`uname -m`/mariadb:latest"
SSL_KEY="/etc/pki/tls/private/http_$(hostname -s).key"
SSL_CERT="/etc/pki/tls/certs/http_$(hostname -s).crt"
MARIADB_ENV="-e MARIADB_USER=zmuser -e MARIADB_DB=zm"
ZM_ENV=""
PULL_ALWAYS=""

ZM_VOLUMES="-v etc_zm:/etc/zm:rw,Z -v var_lib_zoneminder:/var/lib/zoneminder:rw,Z -v var_log_zoneminder:/var/log/zoneminder:rw,Z -v usr_share_zoneminder-common:/usr/share/zoneminder-common:rw,Z -v etc_nginx:/etc/nginx:rw,Z -v var_lib_nginx:/var/lib/nginx:rw,Z -v run_php-fpm:/run/php-fpm:rw,Z -v ${SSL_CERT}:/etc/pki/tls/certs/localhost.crt:ro,Z -v ${SSL_KEY}:/etc/pki/tls/private/localhost.key:ro,Z -v /var/lib/containers/storage/volumes/var_lib_mysql/_data/mysql.sock:/var/lib/mysql/mysql.sock:rw,Z -v /etc/localtime:/etc/localtime:ro"


MARIADB_VOLUMES="-v etc_mariadb:/etc/mariadb:rw,Z -v var_lib_mysql:/var/lib/mysql:rw,Z -v var_log_mariadb:/var/log/mariadb:rw,Z -v usr_share_doc_mariadb:/usr/share/doc/mariadb:rw,Z -v /etc/localtime:/etc/localtime:ro"

[ "$REGISTRY" == "localhost" ] || PULL_ALWAYS="--pull=always"

#[ -d $VOL_PREFIX ] || mkdir -vp $VOL_PREFIX

podman pod exists zm ||  podman pod create --name zm --share uts,ipc,net -p 443:443 -p 80:80 -p 8443:8443 -p 3306:3306 

podman container exists mariadb ||  { podman run --pod=zm -d --log-driver=journald --name mariadb $PULL_ALWAYS --label "io.containers.autoupdate=image" ${MARIADB_ENV} ${MARIADB_VOLUMES} ${MARIADB_IMG} && sleep 30 ; }

# Extract the Mariadb passwords from the otuput of the customization script
for CURL in $(cat /var/lib/containers/storage/volumes/etc_mariadb/_data/dbauth.txt); do 
    echo ${CURL}
    ZM_ENV="${ZM_ENV} -e ${CURL}"
done 
echo ${ZM_ENV}

podman container exists zoneminder || podman run --shm-size=2G --pod=zm -d --log-driver=journald --name zoneminder $PULL_ALWAYS --label "io.containers.autoupdate=image" --device /dev/dri/:/dev/dri:rwm ${ZM_ENV} ${ZM_VOLUMES} ${ZM_IMG}

podman generate systemd --new --restart-policy=always -t 1 --files --name zm

#for CURS in container-mariadb.service container-httpd.service container-zoneminder.service pod-zm.service; do
for CURS in pod-zm.service; do
    cp -vfu ${CURS} /usr/lib/systemd/system/
    systemctl daemon-reload
    systemctl enable ${CURS}
done

