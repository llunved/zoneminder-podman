ARG OS_RELEASE=35
ARG OS_IMAGE=fedora-minimal:$OS_RELEASE

FROM $OS_IMAGE as build

ARG OS_RELEASE
ARG OS_IMAGE
ARG DEVBUILD="False"
ARG HTTP_PROXY=""

LABEL MAINTAINER riek@llunved.net

ENV LANG=C.UTF-8
#ENV VOLUMES="/etc/zoneminder,/var/lib/zoneminde,/usr/share/zoneminder,/var/log/zoneminder,/var/www,/etc/nginx,/var/lib/nginx,/var/log/nginx,/var/php-fpm"
ENV VOLUMES="/etc/zm,/var/lib/zoneminder,/var/log/zoneminder,/etc/nginx,/var/lib/nginx,/var/log/nginx"


USER root

WORKDIR /root

ENV http_proxy=$HTTP_PROXY

RUN dnf -y install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$OS_RELEASE.noarch.rpm \
    https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$OS_RELEASE.noarch.rpm \
    && dnf -y upgrade 

RUN sed -i -e 's/tsflags=nodocs/#tsflags=nodocs/' /etc/dnf/dnf.conf

ADD ./rpmreqs-rt.txt ./rpmreqs-dev.txt /root/
# Create the minimal target environment

RUN dnf install -y glibc-minimal-langpack $(cat rpmreqs-rt.txt) ; \
    if [ "${DEVBUILD}" == "True" ]; then \ 
       dnf -y install \ 
              https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$OS_RELEASE.noarch.rpm \
              https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$OS_RELEASE.noarch.rpm && \
       dnf install --setopt install_weak_deps=false --nodocs -y $(cat rpmreqs-dev.txt); \ 
    fi ; \
    rm -rf /var/cache/*

#FIXME this needs to be more elegant
RUN ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime

# We won't run Mariadb in the same container
RUN sed -i -e "s/^Requires=mariadb.service/#Requires=mariadb.service/g" /usr/lib/systemd/system/zoneminder.service

# Set up the default config for nginx
RUN  ln -sf /etc/zm/www/zoneminder.nginx.conf /etc/nginx/conf.d/ && \
     ln -sf /etc/zm/www/redirect.nginx.conf /etc/nginx/default.d/

# Add some additional files for Zoneminder devices
ADD FI8918W.pm /usr/share/perl5/vendor_perl/ZoneMinder/Control/
# Move the zoneminder config to a deoc dir, so we can mount config from the host but export the defaults from the host
RUN if [ -d /usr/share/doc/zoneminder-common ]; then \
       mv /usr/share/doc/zoneminder-common /usr/share/doc/zoneminder-common.default ; \
    else \
       mkdir -p /usr/share/doc/zoneminder-common.default ; \
    fi ; \
    mkdir /usr/share/doc/zoneminder-common.default/config

RUN for CURF in ${VOLUMES} ; do \
    if [ -d ${CURF} ] && [ "$(ls -A ${CURF})" ]; then \
        mkdir -pv /usr/share/doc/zoneminder-common.default/config${CURF} ; \
        mv -fv ${CURF}/* /usr/share/doc/zoneminder-common.default/config${CURF}/ ;\
    fi ; \
    done

ADD ./chown_dirs.sh \
    ./init_container.sh \
    /sbin
 
RUN chmod +x \
    /sbin/chown_dirs.sh \
    /sbin/init_container.sh 
 
# Set up systemd inside the container
ADD init_container.service \
    chown_dirs.service \
    /etc/systemd/system

RUN systemctl mask systemd-remount-fs.service dev-hugepages.mount sys-fs-fuse-connections.mount systemd-logind.service getty.target console-getty.service && \
    systemctl disable dnf-makecache.timer dnf-makecache.service
RUN /usr/bin/systemctl enable zoneminder.service nginx.service php-fpm.service chown_dirs.service init_container.service fcgiwrap@nginx.socket

ENV CHOWN=true 
ENV CHOWN_DIRS="/var/log/zoneminder,/var/lib/zoneminder,/var/log/nginx,/var/lib/nginx" 
ENV CHOWN_USER="nginx"
 
VOLUME /etc/zm
VOLUME /var/lib/zoneminder
#VOLUME /usr/share/zoneminder
VOLUME /usr/share/doc/zoneminder-common
VOLUME /var/log/zoneminder
VOLUME /etc/nginx
VOLUME /var/lib/nginx
VOLUME /var/log/nginx
VOLUME /var/php-fpm

 
EXPOSE 80 443
CMD ["/usr/sbin/init"]
STOPSIGNAL SIGRTMIN+3

