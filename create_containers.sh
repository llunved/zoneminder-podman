#!/bin/bash

REGISTRY=${REGISTRY:-"edmobuttes.int.bos.llnvd.net"}
HWARCH=${HWARCH:-`uname -m`}
HTTPD_IMG="${REGISTRY}/`uname -m`/httpd:latest"
ZM_IMG="${REGISTRY}/`uname -m`/zoneminder:latest"
MARIADB_IMG="${REGISTRY}/`uname -m`/mariadb:latest"
SSL_KEY='/etc/pki/tls/private/httpd.key'
SSL_CERT='/etc/pki/tls/certs/httpd.crt'
MARIADB_ENV="-e MARIADB_USER=zmuser -e MARIADB_DB=zm"
ZM_ENV=""

#HTTPD_VOLUMES="-v etc_httpd:/etc/httpd:rw,Z -v usr_share_httpd:/usr/share/httpd:rw,Z -v var_www:/var/www:rw,Z -v var_log_httpd:/var/log/httpd:rw,Z -v usr_share_doc_httpd_config:/usr/share/doc/httpd/config:rw,Z -v usr_share_zoneminder:/usr/share/zoneminder:ro,Z -v run_php-fpm:/run/php-fpm:rw,Z -v /etc/localtime:/etc/localtime:ro,Z"

#ZM_VOLUMES="-v etc_zoneminder:/etc/zoneminder:rw,Z -v usr_share_zoneminder:/usr/share/zoneminder:rw,Z -v var_lib_zoneminder:/var/lib/zoneminder:rw,Z -v var_log_zoneminder:/var/log/zoneminder:rw,Z -v usr_share_doc_zoneminder:/usr/share/doc/zoneminder:rw,Z -v etc_httpd:/etc/httpd:rw,Z -v var_www:/var/www:rw,Z -v run_php-fpm:/run/php-fpm:rw,Z -v /etc/localtime:/etc/localtime:ro,Z"
ZM_VOLUMES="-v etc_zm:/etc/zm:rw,Z -v var_lib_zoneminder:/var/lib/zoneminder:rw,Z -v var_log_zoneminder:/var/log/zoneminder:rw,Z -v usr_share_zoneminder-common:/usr/share/zoneminder-common:rw,Z -v etc_nginx:/etc/nginx:rw,Z -v var_lib_nginx:/var/lib/nginx:rw,Z -v run_php-fpm:/run/php-fpm:rw,Z -v ${SSL_CERT}:/etc/pki/tls/certs/localhost.crt:ro,Z -v ${SSL_KEY}:/etc/pki/tls/private/localhost.key:ro,Z -v /var/lib/containers/storage/volumes/var_lib_mysql/_data/mysql.sock:/var/lib/mysql/mysql.sock:rw,Z"
#"-v /etc/localtime:/etc/localtime:ro,Z"

MARIADB_VOLUMES="-v etc_mariadb:/etc/mariadb:rw,Z -v var_lib_mysql:/var/lib/mysql:rw,Z -v var_log_mariadb:/var/log/mariadb:rw,Z -v usr_share_doc_mariadb:/usr/share/doc/mariadb:rw,Z" 
# -v /etc/localtime:/etc/localtime:ro,Z"


#mkdir -p $VOL_PREFIX/etc/httpd $VOL_PREFIX/var/www $VOL_PREFIX/var/www  $VOL_PREFIX/usr/share/doc/httpd $VOL_PREFIX/usr/share/httpd $VOL_PREFIX/var/log/httpd

#podman pod exists zm ||  podman pod create --name zm  -p 443:443 -p 80:80 -p 8443:8443 
#--share net,ipc

#podman run --pod=zm -ti --rm --log-driver=journald --pull=always --name httpd_init ${HTTPD_VOLUMES} ${HTTPD_IMG} /sbin/init_container.sh
#podman run --pod=zm -ti --rm --log-driver=journald --pull=always --name zm_init ${ZM_VOLUMES} ${ZMIMG} /sbin/init_container.sh

#podman run --pod=zm -d --log-driver=journald --pull=always --name httpd --label "io.containers.autoupdate=image" ${HTTPD_VOLUMES} ${HTTPD_IMG}
#podman run --pod=zm -d --log-driver=journald --name httpd --label "io.containers.autoupdate=image" ${HTTPD_VOLUMES} ${HTTPD_IMG}
#podman run --pod=zm -d --log-driver=journald --pull=always --name zm --label "io.containers.autoupdate=image" ${ZM_VOLUMES} ${ZM_IMG}
podman container exists mariadb ||  podman run -d -p 3306:3306 --log-driver=journald --name mariadb --label "io.containers.autoupdate=image" ${MARIADB_ENV} ${MARIADB_VOLUMES} ${MARIADB_IMG}
sleep 30

# Extract the Mariadb passwords from the otuput of the customization script
for CURL in $(cat /var/lib/containers/storage/volumes/etc_mariadb/_data/dbauth.txt); do 
    echo ${CURL}
    ZM_ENV="${ZM_ENV} -e ${CURL}"
done 
echo ${ZM_ENV}

podman container exists zoneminder || podman run --shm-size=2G -p 443:443 -d --device /dev/dri/:/dev/dri:rwm --log-driver=journald --name zoneminder --label "io.containers.autoupdate=image" ${ZM_ENV} ${ZM_VOLUMES} ${ZM_IMG}

podman generate systemd --new --restart-policy=always -t 1 --files --name mariadb
podman generate systemd --new --restart-policy=always -t 1 --files --name zoneminder

#for CURS in container-mariadb.service container-httpd.service container-zoneminder.service pod-zm.service; do
for CURS in container-mariadb.service container-zoneminder.service ; do
    cp -vfu ${CURS} /usr/lib/systemd/system/
    rm -fv ${CURS}
    systemctl daemon-reload
    systemctl enable ${CURS}
done

